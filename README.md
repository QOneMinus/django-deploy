# PosgtreSQL role for ansible

Before using you need up vagrant machines with following command:

    vagrant up

To communicate with machines without a password, you need Set up ssh-agent:

    ssh-agent bash
    ssh-add ~/.ssh/id_rsa

Next step - run playbook for setup and creates database:

    ansible-playbook -i <inventory-file> db_servers.yml

Where *inventory-file* can be **production** or **staging**.
